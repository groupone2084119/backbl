package org.backLivery.m1products.factory;

import org.backLivery.m1products.factory.model.PalettRecyclable;
import org.backLivery.m1products.factory.model.Recyclable;
import org.springframework.stereotype.Component;

@Component
public class PalettFactory implements RecyclableFactory{
    @Override
    public Recyclable create() {
        return new PalettRecyclable();
    }
}
