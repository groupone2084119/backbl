package org.backLivery.m1products.factory;

import org.backLivery.m1products.factory.model.Recyclable;

public interface RecyclableFactory {

    Recyclable create();
}
