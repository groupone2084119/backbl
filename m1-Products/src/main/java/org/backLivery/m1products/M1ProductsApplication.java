package org.backLivery.m1products;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class M1ProductsApplication {

	public static void main(String[] args) {
		SpringApplication.run(M1ProductsApplication.class, args);
	}

}
